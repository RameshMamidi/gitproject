package basics;

public class Operators {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		boolean b1 = true && false;
		System.out.println(b1);
		
	//	boolean b2 = true && 10;   //both should be boolean
		// instead of placing 10 we should write 10>2 or whatever the int should convert into boolean
      
		boolean b3 = true && 10>2; //10>2 which is true..true&&true become true
		System.out.println(b3);
		
		boolean b4 = b3 && false && true; 
		System.out.println(b4);  //Here we get false because In && truth table when all the 
		                           //stmnts true then it will become true otherwise it will be false
		
		boolean b5 =(b3 && (true && true));
		System.out.println(b5);
		
		boolean b6=(true && (10+5>20-6));
		System.out.println(b6);//true
       
        boolean m1 = true || false;
        System.out.println(m1);
        
        boolean m2 = !b6;   //b6 = true
        boolean m3 = !(m2 ||b6); //m2 =false,b6 = true
        System.out.println(m2);
        System.out.println(m3);
        
        
        
        
        
	
	}
	

}
